;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'portage
  authors "Dima Akater"
  first-publication-year-as-string "2019"
  org-files-in-order '("portage-eselect"
                       "portage-emerge-mode"
                       "portage-trivial"
                       "portage-elisp-set"
                       "portage-transient"
                       "portage-generic"
                       "portage-sync"
                       "portage-navi-modern"
                       "portage-toggle-use"
                       "portage")
  site-lisp-config-prefix "70emacs-"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda () (require 'org-src-elisp-extras)))
