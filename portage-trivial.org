# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: portage-trivial
#+subtitle: Part of the =emacs-portage= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle portage-trivial.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

Lots of this is probably obsolete now.

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'subr-x))
(eval-when-compile (require 'shmu-wrap))        ; is it even needed?
(eval-when-compile (require 'mmxx-macros-applications))
(require 'seq)
(require 'shmu-privileged)
(require 'portage-emerge-mode) ; gentoo-cache is required there
#+end_src

#+begin_src elisp :results none
(defvar sudo-edit-local-method)
#+end_src

* Global Variables
#+begin_src elisp :results none
(defvaralias 'portage-set-names-cache
  'gentoo-cache-set-names)
(defalias 'portage-refresh-set-names-cache
  'gentoo-cache-refresh-set-names)
#+end_src

* Custom
#+begin_src elisp :results none
(defgroup portage nil
  "Interaction with Portage, Gentoo package manager"
  :group 'external)
#+end_src

#+begin_src elisp :results none
(defcustom portage-conf-dir-relative "etc/portage"
  "Relative path to portage configuration directory"
  :type 'directory
  :group 'portage)
#+end_src

* truncate-emerge-catpkgs
#+begin_src elisp :results none
(defun portage-truncate-emerge-catpkgs (catpkgs)
  (if (cdr catpkgs) "<multiple>"
    (car catpkgs)))
#+end_src

* truncate-emerge-args
#+begin_src elisp :results none
(defun portage-truncate-emerge-args (args)
  (let (result)
    (cl-loop for arg in args with shortened = ""
             with appending-arg = "-" with appendable = t
             do
             (setf shortened (cl-macrolet ((string-case/with-y (value &rest clauses)
                                             `(string-case ,value
                                                ,@(cl-loop
                                                   for (s . body) in clauses
                                                   if (stringp s)
                                                   collect (cons `(,s ,(concat s "=y")) body)
                                                   else collect (cons s body)))))
                               (or (string-case/with-y arg
                                     ("--alert" "A")
                                     ("--ask" "a")
                                     ("--buildpkg" "b")
                                     ("--buildpkgonly" "B")
                                     ("--changed-use" "U")
                                     ("--debug" "d")
                                     ("--deep" "D")
                                     ("--emptytree" "e")
                                     ("--fetchonly" "f")
                                     ("--fetch-all-uri" "F")
                                     ("--getbinpkg" "g")
                                     ("--getbinpkgonly" "G")
                                     ("--newuse" "N")
                                     ("--nodeps" "O")
                                     ("--noreplace" "n")
                                     ("--oneshot" "1")
                                     ("--onlydeps" "o")
                                     ("--pretend" "p")
                                     ("--quiet" "q")
                                     ("--select" "w")
                                     ("--tree" "t")
                                     ("--update" "u")
                                     ("--usepkg" "k")
                                     ("--usepkgonly" "K")
                                     ("--verbose" "v")
                                     (otherwise
                                      (nullf appendable)))
                                   ;; shortenable non-appendable go here
                                   )))
             (if appendable (setf appending-arg
                                  (concat appending-arg shortened))
               (when (> (length appending-arg) 1)
                 (push appending-arg result)
                 (setf appending-arg "-"))
               (push arg result)
               (setf appendable t))
             finally (when (> (length appending-arg) 1)
                       (push appending-arg result)))
    (string-join (nreverse result) " ")))
#+end_src

* run-emerge-comint
** Definition
#+begin_src elisp :results none
(defun portage-run-emerge-comint (catpkgs &optional args)
  (let (oneshot pretend)
    (cl-loop for arg in args do
             (string-case arg
               ("--oneshot" (setq oneshot t))
               ("--pretend" (setq pretend t))))
    (shmu-with-privileged-if (not pretend)
      (portage-emerge-start*
       `(emerge ,@args ,@catpkgs)
       (lambda (_) (concat "*emerge " (portage-truncate-emerge-args args)
                           (if args " " "")
                           (portage-truncate-emerge-catpkgs catpkgs)
                           "*"))
       (portage-emerge-get-sentinel oneshot pretend)))))
#+end_src

* depclean
** Definition
#+begin_src elisp :results none
(defun portage-depclean ()
  (interactive)
  (shmu-with-privileged
    (portage-emerge-start*
     `(emerge :depclean)
     (lambda (_)
       (concat "*" "emerge" " "
               "--depclean"
               "*")))))
#+end_src

* TODO maybe: =emerge --info=, =emerge --search=, =emerge --help=
=emerge --info= will be needed for ~portage-report-bug~.

* emerge-preserved-rebuild
** Definition
#+begin_src elisp :results none
(defun portage-emerge-preserved-rebuild ()
  (interactive)
  (shmu-with-privileged
    (portage-emerge-start*
     `(emerge @preserved-rebuild)
     (lambda (_)
       (concat "*" "emerge" " "
               "@preserved-rebuild"
               "*")))))
#+end_src

* simple-world-update-sh-form
** Definition
#+begin_src elisp :results none
(defconst portage-simple-world-update-sh-form
  `(emerge :ask :verbose :update :newuse :deep
           :complete-graph y :with-bdeps y :backtrack 50
           : @world))
#+end_src

* simple-world-update
** Definition
#+begin_src elisp :results none
(defun portage-simple-world-update ()
  (interactive)
  (shmu-with-privileged
    (portage-emerge-start*
     portage-simple-world-update-sh-form
     (lambda (_)
       (concat "*" "emerge" " "
               "<world update>"
               "*")))))
#+end_src
